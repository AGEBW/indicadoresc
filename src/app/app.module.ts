import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';


//rutas
import{ APP_ROUTING } from './app.routes';

//Servicios
import{ ServicesService } from './services.service'



//Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { EnlacesComponent } from './components/enlaces/enlaces.component';
import { PresupuestoComponent } from './components/presupuesto/presupuesto.component';
import { ImplanComponent } from './components/implan/implan.component';
import { InnovacionComponent } from './components/innovacion/innovacion.component';
import { ConsultatodosComponent } from './components/consultatodos/consultatodos.component';
import { GrafipastelComponent } from './components/grafipastel/grafipastel.component';
import { BarraComponent } from './components/barra/barra.component';
import { IndicadorComponent } from './components/indicador/indicador.component';
import { HttpClientModule } from '@angular/common/http';
import { from } from 'rxjs';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    ContactoComponent,
    EnlacesComponent,
    PresupuestoComponent,
    ImplanComponent,
    InnovacionComponent,
    ConsultatodosComponent,
    GrafipastelComponent,
    BarraComponent,
    IndicadorComponent
       

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ChartsModule,
    APP_ROUTING,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }