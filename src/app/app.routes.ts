import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { ImplanComponent } from './components/implan/implan.component';
import { InnovacionComponent } from './components/innovacion/innovacion.component';
import { PresupuestoComponent } from './components/presupuesto/presupuesto.component';
import { EnlacesComponent } from './components/enlaces/enlaces.component';
import { ConsultatodosComponent } from './components/consultatodos/consultatodos.component';
import { IndicadorComponent } from './components/indicador/indicador.component';




const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'contacto', component: ContactoComponent },
    { path: 'implan', component: ImplanComponent },
    { path: 'innovacion', component: InnovacionComponent },
    { path: 'presupuesto', component: PresupuestoComponent },
    { path: 'enlaces', component: EnlacesComponent },
    { path: 'consultatodos', component: ConsultatodosComponent },
    { path: 'indicador', component: IndicadorComponent },
    { path: '**', pathMatch:'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);