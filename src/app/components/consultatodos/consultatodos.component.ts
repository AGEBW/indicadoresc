import { Component } from '@angular/core';

import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import {
  monkeyPatchChartJsLegend,
  monkeyPatchChartJsTooltip
} from 'ng2-charts';


@Component({
  selector: 'app-consultatodos',
  templateUrl: './consultatodos.component.html'
  
})
export class ConsultatodosComponent{

  public barChartOptions: ChartOptions = {
    layout: {
    /**padding: {
        left: -30,
        top: 0,
        right: 0,
        bottom: 0,
      }**/
    },
    responsive: true,
    scales: {

      yAxes: [
        {
          offset: true,
        }
      ],
      xAxes: [
        {
          ticks: {
            fontColor: "#fff",
            fontSize: 1,
            lineHeight: 7,

          },
          gridLines: {
            lineWidth: .6,
            zeroLineColor: "#333",
            zeroLineWidth: 1,
          }
        }
      ]
    },

    legend: {
      position: "top",
      align: "center",
      labels: { boxWidth:0 },
    },
    tooltips: {
      enabled:false
    }
  };

  public barChartType: ChartType = 'horizontalBar';

  public barChartLegend = true;



  public barChartData: ChartDataSets[] = [
    { barThickness: 100, maxBarThickness: 101, label: '100%', data: [100.0], stack: 'a' }
  ];



  public colors: any = [
    {
      borderColor: "#fff",
      backgroundColor: ["#005CAB"]
    }
  ];

  constructor() {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }
  ngOnInit() {
  }

}
