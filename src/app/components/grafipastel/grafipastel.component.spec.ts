import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrafipastelComponent } from './grafipastel.component';

describe('GrafipastelComponent', () => {
  let component: GrafipastelComponent;
  let fixture: ComponentFixture<GrafipastelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrafipastelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrafipastelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
