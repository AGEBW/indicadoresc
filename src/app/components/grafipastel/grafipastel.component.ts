/*import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grafipastel',
  templateUrl: './grafipastel.component.html',
  styleUrls: ['./grafipastel.component.css']
})
export class GrafipastelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}*/

import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import {
  SingleDataSet,
  Label,
  monkeyPatchChartJsLegend,
  monkeyPatchChartJsTooltip
} from 'ng2-charts';

@Component({
  selector: "app-grafipastel",
  templateUrl: "./grafipastel.component.html",
  styleUrls: ["./grafipastel.component.css"]
})
export class GrafipastelComponent{
  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: "right",
      align: "center",
      labels: { boxWidth: 30, fontSize: 18, usePointStyle: true }
    }
  };

  public colors: any = [
    {
      borderColor: "#fff",
      backgroundColor: ["#FBA934", "#D94967", "#005CAB"]
      //pointBackgroundColor: '#fff',
      //pointBorderColor: '#fff'
    }
  ];

  public pieChartLabels: Label[] = [
    ["Mayor", "80%"],
    ["Entre", "50% y 80%"],
    ["Menor", "50%"]
  ];
  //public pieChartData: SingleDataSet = [63.49, 7.94, 28.57];
  public pieChartData: SingleDataSet = [63.49, 7.94, 28.57];
  public pieChartType: ChartType = "pie";
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor() {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit() {}
}