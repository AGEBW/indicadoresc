import { Component } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import {
    monkeyPatchChartJsLegend,
    monkeyPatchChartJsTooltip
} from 'ng2-charts';



@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent {

//}


/**@Component({
    selector: 'app-barra',
    templateUrl: './home.component.html',
    styleUrls: ['./barra.component.css']
})**/
//export class BarraComponent {
    public barChartOptions: ChartOptions = {
        responsive: true,
        layout: {
            padding: {
                left: 20,
                top: 0,
                right: 20,
                bottom: 0,
            }
        },
        
        scales: {

            yAxes: [
                {
                    offset: true,
                }
            ],
            xAxes: [
                {
                    ticks: {
                        fontColor: "#333",
                        fontSize: 12,
                        lineHeight: 2.5,

                    },
                    gridLines: {
                        lineWidth: 4,
                        zeroLineColor: "#333",
                        zeroLineWidth: 2.5,
                    }
                }
            ]
        },

        legend: {
            position: "top",
            align: "center",
            labels: { usePointStyle: true },
        },
    };

    public barChartType: ChartType = 'horizontalBar';
        
    public barChartLegend = true;



    public barChartData: ChartDataSets[] = [
        { barThickness: 100, maxBarThickness: 101, label: 'Mayor 80%', data: [63.49], stack: 'a' },
        { barThickness: 100, maxBarThickness: 101, label: 'Entre 50% y 80%', data: [7.94], stack: 'a' },
        { barThickness: 100, maxBarThickness: 101, label: 'Menor a 50%', data: [28.57], stack: 'a' },
    ];



    public colors: any = [
        {
            borderColor: "#fff",
            backgroundColor: ["#fba934"]
        },
        { backgroundColor: ["#005cab"] },
        { backgroundColor: ["#d94967"] },
    ];

    constructor() {
        monkeyPatchChartJsTooltip();
        monkeyPatchChartJsLegend();
    }
    ngOnInit() {
    }

}





/**ASÍ ESTABA ORIGINALMENTE**/
/**import { Component } from '@angular/core';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent {

}**/