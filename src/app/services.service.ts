import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {


  constructor(private http: HttpClient, ) { }

  async IndicadoresTodos() {
    let res = await this.http.get('https://indicadores.heteria.com.mx/api/indicadores',{
      headers:{
        'Access-Control-Allow-Origin':'*',
      }
    }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    });

    return res;
  }

  async IndiCategorias(codigo) {
    let res = await this.http.get('https://indicadores.heteria.com.mx/api/indicadores/categorias/' + codigo,{
      headers:{
        'Access-Control-Allow-Origin':'*',
      }
    }).toPromise().then(data => {
      return data;
    });

    return res;
  }

  async IndiDirecciones(codigo) {
    let res = await this.http.get('https://indicadores.heteria.com.mx/api/indicadores/direcciones/' + codigo,{
      headers:{
        'Access-Control-Allow-Origin':'*',
      }
    }).toPromise().then(data => {
      return data;
    });

    return res;
  }

  async IndicadoresFiltro(direccion , categoria) {
    let res = await this.http.get('https://indicadores.heteria.com.mx/api/indicadores/filtros/' + direccion + '/' + categoria,{
      headers:{
        'Access-Control-Allow-Origin':'*',
      }
    }).toPromise().then(data => {
      return data;
    });

    return res;
  }

}